package com.xfdmao.fcat.coin.huobi.response;

public class Symbol {

    public String baseCurrency;
    public String quoteCurrency;
    public String symbol;

}
